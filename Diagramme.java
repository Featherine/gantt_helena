// import java.io.BufferedReader;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.io.InputStreamReader;
// import java.io.PrintWriter;
import java.io.FileWriter;
import java.util.Scanner;

// import Io; 

// import java.util.Arrays;
// import java.util.ArrayList;

// import java.io.* ;

public class Diagramme {






   // public static void pushIn( String tache, String listeTache, String midDate){

   //    try{
   //       /*Save task's informations in a .txt file*/
   //       String filePath = "DiagrammeGantt.txt";
   //       FileWriter fw = new FileWriter(filePath, true);        
   //       fw.write(tache + ";" + midDate + ";" + listeTache);
   //       fw.write("\n");
   //       fw.close();
   //    }

   //    catch (Exception e){
   //       System.out.println(e);
   //    }
   //     System.out.println("Kakimashita !");
      
   // }




   public static void checkProchaineTache(){

      String suite;
      do {
         /*this method asks if there is an other task to register in the gantt diagramm*/
         Scanner suivant = new Scanner(System.in);
         System.out.println("Next task ? y / n"); 
         suite = suivant.next();
      suivant.close();
      }
      while (!suite.equalsIgnoreCase("y") && !suite.equalsIgnoreCase("n"));

      if (suite.equalsIgnoreCase("y")) {
         task();         
      }

      else{
         System.out.println("All the tasks are registred");
      }
      

   }





   public static void checkAnswerOtherTask(String repAutreTache) {

       while (!repAutreTache.equalsIgnoreCase("y") && !repAutreTache.equalsIgnoreCase("n")){

         System.out.println("I didn't understand.");
         Scanner questionAutreTacheRepeat = new Scanner(System.in);
         System.out.println("Is another task must be finished before ? y / n :");
         repAutreTache = questionAutreTacheRepeat.next();
         questionAutreTacheRepeat.close();
      }
   }




   public static void task() {

      /*Recuperation of the informations to constitute a Gantt diagramm*/
      Scanner quelleTache = new Scanner(System.in);
      System.out.println("Task name ?:");
      String tache = quelleTache.next();
      quelleTache.close();

   

      Scanner dureeMoy = new Scanner(System.in);
      System.out.println("Estimation to accomplish the task ?:");
      String midDate = dureeMoy.next();
      dureeMoy.close();



      /*Collect informations about previously tasks who eventually need to be finished*/
      Scanner questionTache = new Scanner(System.in);
      System.out.println("Should a task must be finished before ? y / n");
      String repTache= questionTache.next();
      questionTache.close();
      String tacheDep;

      while (!repTache.equalsIgnoreCase("y") && !repTache.equalsIgnoreCase("n")){
         Scanner questionTacheRepeat = new Scanner(System.in);
         System.out.println("Should another task must be finished before ? y / n");
         repTache= questionTacheRepeat.next();
         questionTacheRepeat.close();
      }

      PushIn push = new PushIn();

      if (repTache.equalsIgnoreCase("y")) {

         Scanner tacheDependante = new Scanner(System.in);
         System.out.println("Which one ? :");
         tacheDep = tacheDependante.next();
         tacheDependante.close();
         
         
         push.pushIn(tache, tacheDep, midDate);

         // pushIn(tache, tacheDep, midDate);

         Scanner questionAutreTache = new Scanner(System.in);
         System.out.println("Is another task must be finished before ? y / n :");
         String repAutreTache = questionAutreTache.next();
         questionAutreTache.close();
            
         checkAnswerOtherTask(repAutreTache);

          while (!repAutreTache.equalsIgnoreCase("n")){

            Scanner tacheDependanteDeux = new Scanner(System.in);
            System.out.println("Which one ? :");
            String tacheDepDeux = tacheDependante.next();
            tacheDependanteDeux.close();
            tacheDep = tacheDepDeux;

            push.pushIn(tache, tacheDep, midDate);
            // pushIn(tache, tacheDep, midDate);

            Scanner questionAutreTacheDeux = new Scanner(System.in);
            System.out.println("Is another task must be finished before ? y / n :");
            repAutreTache = questionAutreTacheDeux.next();
            questionAutreTacheDeux.close();

            checkAnswerOtherTask(repAutreTache);

         }

      }

      else if (repTache.equalsIgnoreCase("n")) {

         tacheDep ="0";
         push.pushIn(tache, tacheDep, midDate);
         // pushIn(tache, tacheDep, midDate);
         
         
      }

   /*Redirect to the method which asks if there is another task*/
      checkProchaineTache();
      
   }




    public static void main(String[] arg) {      
      
      /*Call the task function to begin all the processus*/
      task();

   }

}
